<!DOCTYPE html>
<html>
<head>
	<title>AsF - Procurar Grupos</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/search.css">
	<link rel="icon" href="Imagens/apx2.png" type="image/x-icon" />
</head> 
<body>

		<header>
			
			<div id="cabecalho">
				
				<div id="cabecalho_logo">
					<a href="index.html"> APEX SQUAD FINDER</a>
				</div>

				<ul id="cabecalho_menu">
					<li><a href="index.html">Home</a></li>
					<li><a href="suporte.html">Suporte</a></li>
					<li><a href="sobre.html">Sobre</a></li>
					<li><a href="login-cadastro.html">Login/Cadastro</a></li>
				</ul>
				
			</div>
</header>

		<main>

				<div> <!--   DIV LOGO -->
					<img id="imgmenustye" src="Imagens/ApexCriar.png">
		</div>
				<div id="principal"> <!--   DIV BACKGROUND PRINCIPAL WHITE -->
						
					<div id="filtrarpesquisa-fundo">  <!--   DIV FILTRAR PESQUISA -->

						<div>
							<h2 id="findermenu">FILTRAR GRUPOS</h2>
							<hr>
						</div>

						<div class="config">							
							Plataforma
		</div>

						<div class="config2">							
							Origin ID
		</div>

						<div class="config">							
							Personagem
		</div>

						<div class="config">							
							Nível
		</div>
<br>
						<div class="config-plat">
						<select style="width: 90px"> 

							<option>PC</option>		
							<option>Ps4</option>
							<option>Xbox</option>

						</select>							
							
		</div>

						<div class="config-id">	
							
							<input type="text" name="Origin ID" maxlength="30" style="width: 100px;">							
							
		</div>

						<div class="config-pers">	
						<select> 

							<option>Bangalore</option>		
							<option>Bloodhound</option>
							<option>Caustic</option>
							<option>Gibraltar</option>
							<option>Lifeline</option>
							<option>Mirage</option>
							<option>Octane</option>
							<option>Pathfinder</option>
							<option>Wraith</option>

						</select>							
							
		</div>

						<div class="config-nv">
						<select> 

							<option>1-20</option>		
							<option>21-40</option>
							<option>41-60</option>
							<option>61-80</option>
							<option>81-100</option>

						</select>							
							
		</div>

						<div class="config-bt">
							
							<form method="get" action=" index.html"><button type="submit" id="buttonstyle">FILTRAR</button></form>							
							
		</div>

					
	</div>
				<div id="resultxt"> 
					<h2>RESULTADOS</h2>
		</div>

<div id="resultado-box">
	

</div>



</div>


</main>



		<footer>
	

			<div id="rodape">

				<div id="listarodape">
					<ul id="listtype">
						<li ><a href="">Política de privacidade</a></li>
						<li><a href="">Política de cookies</a></li>
						<li><a href="">Termos de Serviço</a></li>
					</ul>

				</div>
				
				<p>Todos os direitos reservados</p>
				<p>Copyright © 2019 de Apex Squad Finder Team</p>
			</div>


</footer>


</body>
</html>