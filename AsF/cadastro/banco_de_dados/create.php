<?php
session_start();
include_once 'conexao.php';

$nome      = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_SPECIAL_CHARS);
$email     = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
$originid  = filter_input(INPUT_POST, 'originid', FILTER_SANITIZE_SPECIAL_CHARS);

$querySelect = $connect->query("select email from tb_clientes");
$array_emails = [];
#Verificar se ja existe um email existente no banco de dados igual ao que está tentando ser incluído
while($emails = $querySelect->fetch_assoc()):
    $emails_existentes = $emails['email'];
    array_push($array_emails, $emails_existentes);
endwhile;

if(in_array($email, $array_emails)):
    $_SESSION['msg'] = "<p class= 'center red-text'>".'Já existe um perfil cadastrado com esse email'."</p>";
    header("Location:../");
else:
    $queryInsert = $connect->query("insert into tb_clientes values(default,'$nome','$email','$originid')");
    $affected_rows = mysqli_affected_rows($connect);
    
    if($affected_rows > 0):
        $_SESSION['msg'] = "<p class = 'center green-text'>".'Cadastro efetuado com sucesso!'."</p>";
        header("Location:../");
    endif;
endif;
    