<?php session_start()?>
<!DOCTYPE html>
<html>
<head>
	<title>AsF-Registro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/search.css">
	<link rel="icon" href="Imagens/apx2.png" type="image/x-icon" />
        <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
      <!--CSS DO MATERIALIZE-->
      <link rel="stylesheet" href="materialize/css/materialize.min.css">
      
      
</head> 
<body>
            <!--Arquivos Jquery e JavaScrifpt-->
            <script type="text/javascript" src="materialize/js/jquery-3.4.1.min.js"></script>
            <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
            
            <!-- Inicialização Jquery-->
            <script type="text/javascript">
                $(document).ready(function(){
                    
                });
            </script>
		<header>
			
			<div id="cabecalho">
				
				<div id="cabecalho_logo">
					<a href="../index.php"> APEX SQUAD FINDER</a>
				</div>

				<ul id="cabecalho_menu">
					<li><a href="../index.php">Home</a></li>
					<li><a href="../suporte.html">Suporte</a></li>
					<li><a href="../sobre.html">Sobre</a></li>
                                        <li><a href="index.php"><i class="material-icons left">account_circle</i>Cadastro</a></li>
                                        <li><a href="consultas.php"><i class="material-icons left">search</i>Consultas</a></li>
                                        
				</ul>
                           

				
			</div>
                    
</header>
    
    
    
		<main>

				<div> <!--   DIV LOGO -->
					<img id="imgmenustye" src="Imagens/ApexCriar.png">
		</div>

		<!--Formulario de cadastro-->
                <div class="row container">
                    
                    <form action="banco_de_dados/create.php" method="post" class="col s12">
                        <fieldset class="formulario">
                            <legend><img src="Imagens/perfil.png" alt="(imagem)"width="100"></legend>
                            <h5 class="light center">Cadastro de perfil</h5>
                           
                            <?php
                            #retornar se deu certo ou errado
                            if(isset($_SESSION['msg']));
                                echo $_SESSION['msg'];
                               session_unset();
                               
                            ?>
                            
                            <!--Campo nome-->
                            <div class="input-field col s12">
                                <i class="material-icons prefix">account_circle</i>
                                <input type="text" name="nome" id="nome" maxlength="40" required autofocus>
                                <label for="nome">Nome do Perfil</label>
                                    
                            </div>
                            
                            <!--Campo email-->
                            <div class="input-field col s12">
                                <i class="material-icons prefix">email</i>
                                <input type="email" name="email" id="email" maxlength="50" required >
                                <label for="email">Email do Perfil</label>
                                    
                            </div>
                            <!--Campo OrigiID-->
                            <div class="input-field col s12">
                                <i class="material-icons prefix">account_circle</i>
                                <input type="text" name="originid" id="originid" maxlength="30" required >
                                <label for="originid">Origin ID do Perfil</label>
                                    
                            </div>
                            
                            <!--Botoes-->
                            <div class="input-field col s12">
                                <input type="submit" value="cadastrar" class="btn blue">
                                <input type="reset" value="limpar" class="btn red">
                            </div>
                            
                        </fieldset>
                    </form>
                    
                    
                </div>
              



                </main>


		<footer>
	

			<div id="rodape">

				<div id="listarodape">
					<ul id="listtype">
						<li ><a href="">Política de privacidade</a></li>
						<li><a href="">Política de cookies</a></li>
						<li><a href="">Termos de Serviço</a></li>
					</ul>

				</div>
				
				<p>Todos os direitos reservados</p>
				<p>Copyright © 2019 de Apex Squad Finder Team</p>
			</div>


</footer>


</body>
</html>

?>