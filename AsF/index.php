
<?php  

$chamados = array();

$arquivo = fopen('registro_partidas.txt', 'r');

while(!feof($arquivo)){
	
	$registro = fgets($arquivo);
	$chamados[] = $registro;
	
}

fclose($arquivo);

?>

<!DOCTYPE html>
<html>
<head>
	<title>AsF - Home</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/index.css">
	<link rel="icon" href="Imagens/apx2.png" type="image/x-icon" />
</head> 
<body>

		<header>
			
			<div id="cabecalho" >
				
				<div id="cabecalho_logo">
					<a href="index.php"> APEX SQUAD FINDER</a>
				</div>

				<ul id="cabecalho_menu">
					<li class="selecionado"><a href="index.php">Home</a></li>
					<li><a href="suporte.html">Suporte</a></li>
					<li><a href="sobre.html">Sobre</a></li>
					<li><a href="cadastro/index.php">Login/Cadastro</a></li>
				</ul>
				
			</div>
</header>

		<main>

				<div> <!--   DIV LOGO -->
					<img id="imgmenustye" src="Imagens/Apex-logo1.png">
		</div>

					<!--   DIV BEM VINDO -->
				<div id="txtmeio"> 
					<h1 style="padding: 5px">BEM VINDO AO APEX - SQUAD FINDER!</h1>
		</div>


				<div id="principal"> <!--   DIV BACKGROUND PRINCIPAL WHITE -->
						
					<div id="filtrarpesquisa-fundo">  <!--   DIV FILTRAR PESQUISA -->

						<div>
							<h2 id="findermenu">Filtrar Pesquisa</h2>
							<hr>
						</div>

						<div class="config">							
							Plataforma
		</div>

						<div class="config2">							
							Origin ID
		</div>

						<div class="config">							
							Personagem
		</div>

						<div class="config">							
							Nível
		</div>
<br>
						<div class="config-plat">
						<select style="width: 90px"> 

							<option>PC</option>		
							<option>Ps4</option>
							<option>Xbox</option>

						</select>							
							
		</div>

						<div class="config-id">	
							
							<input type="text" name="Origin ID" maxlength="30" style="width: 100px;">							
							
		</div>

						<div class="config-pers">	
						<select> 

							<option>Bangalore</option>		
							<option>Bloodhound</option>
							<option>Caustic</option>
							<option>Gibraltar</option>
							<option>Lifeline</option>
							<option>Mirage</option>
							<option>Octane</option>
							<option>Pathfinder</option>
							<option>Wraith</option>

						</select>							
							
		</div>

						<div class="config-nv">
						<select> 

							<option>1-20</option>		
							<option>21-40</option>
							<option>41-60</option>
							<option>61-80</option>
							<option>81-100</option>

						</select>							
							
		</div>

						<div class="config-bt">
							
							<button name="button" style="padding: 3px 4px; font-family: Comic Sans MS, cursive, sans-serif;">Filtrar</button>							
							
		</div>

					
	</div>


			<div id="criarpartida">


				<a href="criar-partida.php"><h3>Criar partida</h3></a>
				


			</div>

				<div id="resultxt"> 
					<h2>Resultados</h2>
		</div>


<?php foreach($chamados as $value) { ?>

	<?php 
	$chamado_dados = explode('#', $value);

	if (count($chamado_dados) < 5) {
		continue;
	}

	?>
<div id="resultado-box">
	<div style="background-color: #474746; text-align: center;">
		<h3 class="formt">ID: <?= $chamado_dados[0] ?> </h3>  <!-- ID  -->
		<h3 class="formt">Plat: <?= $chamado_dados[1] ?></h3> <!-- PLAT  -->
		<h3 class="formt">Pers: <?= $chamado_dados[2] ?></h3> <!-- PERS  -->
		<h3 class="formt">Nv: <?= $chamado_dados[3] ?></h3> <!-- NIVEL  -->
		<h3 class="formt">Voice: <?= $chamado_dados[5] ?> </h3> <!-- VOICE  -->
	</div>
	<hr style="border: 1px solid black; margin-bottom: 10px;">
	<h3 style="padding: 0px 10px 0px 10px;">Informação extra: </h3>
	<p style="padding: 5px 10px;"><?= $chamado_dados[4] ?></p>


</div> <!-- FIM DIV RESULTADO BOX  -->

<?php } ?>


</div>


</main>



		<footer>
	

			<div id="rodape">

				<div id="listarodape">
					<ul id="listtype">
						<li ><a href="">Política de privacidade</a></li>
						<li><a href="">Política de cookies</a></li>
						<li><a href="">Termos de Serviço</a></li>
					</ul>

				</div>
				
				<p>Todos os direitos reservados</p>
				<p>Copyright © 2019 de Apex Squad Finder Team</p>
			</div>


</footer>


</body>
</html>