<!DOCTYPE html>
<html>
<head>
	<title>AsF - Criar Partida</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/criar.css">
	<link rel="icon" href="Imagens/apx2.png" type="image/x-icon" />
</head>

<body>
								<!--COFIGURAÇÃO ~HEADER -->
				<header>
							<div id="cabecalho">
						
						<div id="cabecalho_logo">
							<a href="index.php"> APEX SQUAD FINDER</a>
						</div>

						<ul id="cabecalho_menu">
							<li><a href="index.php">Home</a></li>
							<li><a href="suporte.html">Suporte</a></li>
							<li><a href="sobre.html">Sobre</a></li>
							<li><a href="login-cadastro.html">Login/Cadastro</a></li>
						</ul>
						
					</div>
	

</header>
				<main>

									<div> <!--   DIV LOGO -->
					<img id="imgmenustye" src="Imagens/ApexCriar.png">
		</div>
						
						<div id="principal">
							
							<div id="criarpartidatxt">
								<h1>CRIAR PARTIDA</h1>
							</div>

							<div id="menuCriarpartida">


								<div class="config">							
							Origin ID
		</div>

						<div class="config2">							
							Plataforma
		</div>

						<div class="config">							
							Personagem
		</div>

						<div class="config">							
							Nível
		</div>
		<br>

					<form method="post" action="registrar_partida.php">
						<div class="config-id">	
							
							<input type="text" name="OriginID" maxlength="30" style="width: 100px;">							
							
		</div>

								<div class="config-plat">
						<select name="plat" style="width: 90px"> 

							<option>PC</option>		
							<option>Ps4</option>
							<option>Xbox</option>

						</select>							
							
		</div>

						<div class="config-pers">	
						<select name="pers"> 

							<option>Bangalore</option>		
							<option>Bloodhound</option>
							<option>Caustic</option>
							<option>Gibraltar</option>
							<option>Lifeline</option>
							<option>Mirage</option>
							<option>Octane</option>
							<option>Pathfinder</option>
							<option>Wraith</option>

						</select>							
							
		</div>

						<div class="config-nv">
						<select name="nv"> 

							<option>1-20</option>		
							<option>21-40</option>
							<option>41-60</option>
							<option>61-80</option>
							<option>81-100</option>

						</select>							
							
		</div>

		<br>

						<div class="config-inf">	

							<p style="padding-bottom: 10px">Informação extra</p>
							
		</div>

						<div class="config-comun">	

							<p style="padding-bottom: 10px">Comunicação</p>
							
		</div>
		<br>
						<div class="config-areainf">
							<textarea rows="4" cols="40" name = "areainf" maxlength="300">Escreva uma informação extra</textarea>	
						</div>

						<div class="config-areacomu">
									
								<select name="comu"> 

									<option>Discord</option>
									<option>In-Game</option>
									<option>Outro</option>

								</select>
						</div>
						<br>


								<div class="config-bt">

							<button type="submit" id="buttonstyle">CRIAR</button>						
							
		</div>

		</form>
						<div style="clear: both;">
							
						</div>


	


							</div>



				</div>

</main>

								<!--COFIGURAÇÃO FOOTER -->
				<footer>

			<div id="rodape">

				<div id="listarodape">
					<ul id="listtype">
						<li ><a href="">Política de privacidade</a></li>
						<li><a href="">Política de cookies</a></li>
						<li><a href="">Termos de Serviço</a></li>
					</ul>

				</div>
				
				<p>Todos os direitos reservados</p>
				<p>Copyright © 2019 de Apex Squad Finder Team</p>
			</div>
	

</footer>


</body>
</html>